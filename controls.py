
from flask.views import MethodView
from flask import jsonify, request, abort
from api.models import Citizen, Navid
# from throne.models import storedData, temporaryData
from opensocial.models import *
from opensocial.decorators import *
from opensocial.validations import *
# from opensocial.utils import citizenID_generator
# from throne.settings import king_pub

# from blockchain.block import Block
# import blockchain.sync
from api.utils import citizenID_generator, walletID_generator
from throne.default_vals import *
import json
from Shahi import *
import uuid
from throne.shared import db
#===========================================================================
#===========================================================================





# ==========================================================================
# ==========================================================================
# ==========================================================================

class notify(MethodView):

    decorators = [app_required]

    def post(self):

        order,language,version,timestamp,signature,error_codes,success_flag = validate_notify(request)

        if success_flag:

            notifs_list = []

            if order == 'CTID' or order == 'all':
                CTID = citizenID_generator(request.headers.get('CITIZEN-PKey'))

                last_get = ''
                subcrbr = Subscriber.query.filter_by(c_w_id=CTID).first()
                true_time = False
                if subcrbr:
                    last_get = subcrbr.last_get

                    if (int(subcrbr.last_get[0:14]) < int(timestamp[0:14])):
                        subcrbr.lang = language
                        subcrbr.last_get = timestamp[0:14]
                        true_time = True
                    db.session.commit()
                else:
                    sub = Subscriber(CTID, ShahiDatetime.now().strftime('%Y%m%d%H%M%S%f'), '',language,timestamp[0:14],True)
                    db.session.add(sub)
                    db.session.commit()

                if last_get != timestamp[0:14] and true_time :

                    notifs = Notification.query.filter_by(c_w_id=CTID).all()
                    # TODO: need?
                    if notifs:
                        for nt in notifs:
                            notif_data = {}
                            notif_data['title'] = nt.title
                            notif_data['message'] = nt.message
                            notifs_list.append(notif_data)

                        Notification.query.filter_by(c_w_id=CTID).delete()
                        db.session.commit()

            if order == 'ENID' or order == 'all':
                WLID = walletID_generator(request.headers.get('CITIZEN-PKey'))

                last_get = ''
                subcrbr = Subscriber.query.filter_by(c_w_id=WLID).first()
                true_time = False
                if subcrbr:
                    last_get = subcrbr.last_get

                    if (int(subcrbr.last_get[0:14]) < int(timestamp[0:14])):
                        subcrbr.lang = language
                        subcrbr.last_get = timestamp[0:14]
                        true_time = True
                    db.session.commit()
                else:
                    sub = Subscriber(WLID, ShahiDatetime.now().strftime('%Y%m%d%H%M%S%f'), '',language,timestamp[0:14],True)
                    db.session.add(sub)
                    db.session.commit()

                if last_get != timestamp[0:14] and true_time :

                    notifs = Notification.query.filter_by(c_w_id=WLID).all()
                    # notifs_list = []
                    # TODO: need?
                    if notifs:
                        for nt in notifs:
                            notif_data = {}
                            notif_data['title'] = nt.title
                            notif_data['message'] = nt.message
                            notifs_list.append(notif_data)

                        Notification.query.filter_by(c_w_id=WLID).delete()
                        db.session.commit()


            return jsonify({
                "notifs" : notifs_list,
                "status" : "ok"
                })

            # return jsonify({
            #     "status" : "error",
            #     'message' : "user havent new notif!",
            #     "error_codes" : 420
            # }), 400
        else:
            return jsonify({
                "status" : "error",
                "error_codes" : error_codes
            }), 400
#===========================================================================
#===========================================================================
