from functools import wraps
from flask import request, jsonify
from api.utils import verify_signature, verify_tr_signature

#===========================================================================
#===========================================================================



#===========================================================================
#============================= join_required ===============================
#===========================================================================

def join_required(f):
    @wraps(f)

    def decorated_function(*args,**kwargs):
        error_codes = []
        warning_codes = []
        success_flag = 1
        try:
            input_data = request.json
        except:
            input_data = {}

        if request.headers.get('CITIZEN-PKey') is None or input_data['signature'] is None:
            return jsonify({
                "status" : "error",
                "error_codes" : 21
            }), 403
        else:

            try:
                signature = input_data['signature']

                acceptable_signature = verify_signature(input_data)

                if not acceptable_signature:
                    error_codes.append('0000')
                    success_flag = 0
            except:
                error_codes.append('1')
                success_flag = 0

            if success_flag:
                return f(*args,**kwargs)
            else:
                return jsonify({
                    "status" : "error",
                    "error_codes" : error_codes
                }), 403


    return decorated_function
#===========================================================================
#===========================================================================



#===========================================================================
#============================== app_required ===============================
#===========================================================================


def app_required(f):
    @wraps(f)

    def decorated_function(*args,**kwargs):
        error_codes = []
        warning_codes = []
        success_flag = 1
        try:
            input_data = request.json
        except:
            input_data = {}

        if request.headers.get('CITIZEN-PKey') is None or input_data.get('signature') is None:
            return jsonify({
                "status" : "error",
                "error_codes" : 21
            }), 403
        else:

            try:
                signature = input_data['signature']
                acceptable_signature = verify_signature(request.headers,input_data)

                if not acceptable_signature:
                    error_codes.append('0000')
                    success_flag = 0
            except:
                # raise
                error_codes.append('1')
                success_flag = 0

            if success_flag:
                return f(*args,**kwargs)
            else:
                return jsonify({
                    "status" : "error",
                    "error_codes" : error_codes
                }), 403


    return decorated_function

#===========================================================================
#===========================================================================




#===========================================================================
#============================== app_required ===============================
#===========================================================================


def Tr_required(f):
    @wraps(f)

    def decorated_function(*args,**kwargs):
        error_codes = []
        warning_codes = []
        success_flag = 1
        try:
            input_data = request.json
        except:
            input_data = {}

        if request.headers.get('PUBLIC-KEY') is None or input_data.get('signature') is None:
            return jsonify({
                "status" : "error",
                "error_codes" : 21
            }), 403
        else:

            try:
                signature = input_data['signature']
                acceptable_signature = verify_tr_signature(request.headers,input_data)

                if not acceptable_signature:
                    error_codes.append('0000')
                    success_flag = 0
            except:
                # raise
                error_codes.append('1')
                success_flag = 0

            if success_flag:
                return f(*args,**kwargs)
            else:
                return jsonify({
                    "status" : "error",
                    "error_codes" : error_codes
                }), 403


    return decorated_function

#===========================================================================
#===========================================================================
