from throne.shared import db




# db is SQLAlchemy object
class Subscriber(db.Model):
    __bind_key__ = 'db_Social'

    id = db.Column(db.Integer(), primary_key=True, default=None)
    c_w_id = db.Column(db.String(255), nullable=False)
    created = db.Column(db.String(32))
    subscription_info = db.Column(db.Text())
    lang = db.Column(db.String(32))
    last_get = db.Column(db.String(32))
    is_active = db.Column(db.Boolean(), default=True)
    def __init__(self, c_w_id=None, created=None, subscription_info=None, lang=None, last_get=None, is_active=None):
        self.c_w_id = c_w_id
        self.created = created
        self.subscription_info = subscription_info
        self.lang = lang
        self.last_get = last_get
        self.is_active = is_active
    def __repr__(self):
        return "<{}:{}>".format(self.CTID, self.block)


# db is SQLAlchemy object
class Notification(db.Model):
    __bind_key__ = 'db_Social'

    id = db.Column(db.Integer(), primary_key=True)
    c_w_id = db.Column(db.String(255), db.ForeignKey('subscriber.c_w_id'), nullable=False)
    date = db.Column(db.String(32))
    title = db.Column(db.String(32))
    message = db.Column(db.Text())
    info = db.Column(db.Text())
    action = db.Column(db.String(32))
    type = db.Column(db.String(32))
    isRead = db.Column(db.Boolean(), default=True)
    def __init__(self, c_w_id=None, date=None, title=None, message=None, info=None, action=None, type=None, isRead=None):
        self.c_w_id = c_w_id
        self.date = date
        self.title = title
        self.message = message
        self.info = info
        self.action = action
        self.type = type
        self.isRead = isRead
    def __repr__(self):
        return "<{}:{}>".format(self.CTID, self.block)
