
# from math import sin, cos, sqrt, atan2, radians
# import random
# import binascii
# import os
# import time
# import string
# import hashlib
from throne.settings import ALLOWED_EXTENSIONS





#===========================================================================
#==========================    allowed_file    =============================
#===========================================================================
def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
#===========================================================================
#===========================================================================
