# -*- coding: utf-8 -*-

from throne.default_vals import *
from datetime import datetime, timedelta
from flask import json
from api.utils import isAcceptableTime

#===========================================================================
#============================ validate_join ================================
#===========================================================================
def validate_notify(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    try:
        input_data = request.json
    except:
        input_data = {}


    #-------------------------Validate the Details--------------------------
    try:
        order = input_data['lang']
        if not (order) and not (order == 'CTID' or order == 'ENID'):
            order = 'all'
            # error_codes.append('1')
            # success_flag = 0
    except:
        order = 'all'
        # error_codes.append('1')
        # success_flag = 0

    try:
        language = input_data['lang']
        if not (language):
            language = 'EN'
            # error_codes.append('1')
            # success_flag = 0
    except:
        language = 'EN'
        # error_codes.append('1')
        # success_flag = 0

    try:
        version = input_data['version']
        if not (version):
            error_codes.append('1')
            success_flag = 0
    except:
        version = ''
        error_codes.append('1')
        success_flag = 0



    try:
        timestamp = input_data['timestamp']
        if not (timestamp):
            error_codes.append('1')
            success_flag = 0
    except:
        timestamp = ''
        error_codes.append('1')
        success_flag = 0


    try:
        signature = input_data['signature']
        if not (signature):
            error_codes.append('1')
            success_flag = 0
    except:
        signature = ''
        error_codes.append('1')
        success_flag = 0


    return order,language,version,timestamp,signature,error_codes,success_flag



#===========================================================================
#===========================================================================




#===========================================================================
#============================ validate_join ================================
#===========================================================================
def validate_AddNavid(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    try:
        input_data = json.loads(request.form['json'])
    except:
        input_data = {}


    try:
        photo = request.files['photo']
        # img = Image.open(cover)
        # print("========----------=========")
        # print(cover)
        # img.save( img.filename ,format="JPEG", quality=70)
        # print(img)
        # # cover = img
        # thumb_io = StringIO.StringIO()
        # thumb.save(thumb_io, format='JPEG')
        # cover = InMemoryUploadedFile(img, None, img.filename, 'image/jpeg',None)
    except:
        # raise
        photo = ""




    #-------------------------Validate the Details--------------------------


    #-------------------------Validate the Details--------------------------
    try:
        navid = input_data['navid']
        if not (navid):
            navid = ''
            error_codes.append('1')
            success_flag = 0
    except:
        navid = ''
        error_codes.append('1')
        success_flag = 0


    try:
        to = input_data['to']
        if not (to):
            to = ''
            # error_codes.append('1')
            # success_flag = 0
    except:
        to = ''
        # error_codes.append('1')
        # success_flag = 0


    try:
        link = input_data['link']
        if not (link):
            link = ''
            # error_codes.append('1')
            # success_flag = 0
    except:
        link = ''
        # error_codes.append('1')
        # success_flag = 0







    try:
        version = input_data['version']
        if not (version):
            error_codes.append('1')
            success_flag = 0
    except:
        version = ''
        error_codes.append('1')
        success_flag = 0



    try:
        timestamp = input_data['timestamp']
        if not (timestamp):
            error_codes.append('1')
            success_flag = 0
    except:
        timestamp = ''
        error_codes.append('1')
        success_flag = 0

    #
    # try:
    #     signature = input_data['signature']
    #     if not (signature):
    #         error_codes.append('1')
    #         success_flag = 0
    # except:
    #     signature = ''
    #     error_codes.append('1')
    #     success_flag = 0


    return navid,photo,to,link,version,timestamp,error_codes,success_flag




#===========================================================================
#===========================================================================



#===========================================================================
#========================== validate_GetFeeds ==============================
#===========================================================================
def validate_GetFeeds(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    try:
        input_data = request.json
    except:
        input_data = {}

    #-------------------------Validate the Details--------------------------


    try:
        citizen_id = input_data['citizen_id']
    except:
        citizen_id = None

    try:
        page = input_data['page']
    except:
        page = 1



    try:
        version = input_data['version']
        if not (version):
            error_codes.append('1')
            success_flag = 0
    except:
        version = ''
        error_codes.append('1')
        success_flag = 0


    try:
        timestamp = input_data['timestamp']
        if not (timestamp):
            error_codes.append('1')
            success_flag = 0
    except:
        timestamp = ''
        error_codes.append('1')
        success_flag = 0


    return citizen_id,page,version,timestamp,error_codes,success_flag



#===========================================================================
#===========================================================================
